#Laravel Dusk and Telescope
@(Laravel)[app, laravel]


[![PHP](https://img.shields.io/badge/PHP-7.2-blue.svg)](http://php.net/)
[![Laravel](https://img.shields.io/badge/Laravel-5.8-red.svg)](https://laravel.com/)
[![Telescope](https://img.shields.io/badge/Telescope-1.0-Cyan.svg)](https://laravel.com/docs/master/telescope/)
[![mySql](https://img.shields.io/badge/mySql-5.7-orange.svg)](https://www.mysql.com/)
[![Nginx](https://img.shields.io/badge/nginx-1.14-olive.svg)](http://nginx.org/)
[![selenium](https://img.shields.io/badge/selenium-3.1-green.svg)](http://nginx.org/)

---
### Links
1. [Laravel Dusk](https://laravel.com/docs/master/dusk#using-other-browsers)
2. [Laradock Selenium](https://laradock.io/documentation/#use-selenium)
3. [Selenium failing with laravel dusk](https://github.com/laradock/laradock/issues/1001)



---
### Instaling
1. Up containers `docker-compose up -d nginx mysql redis selenium`
2. Spin up `selenium/nginx/[db]` containers.
2. `php artisan dusk:install`
3. Change: `'http://localhost:9515', DesiredCapabilities::chrome()`
	- to `'http://selenium:4444/wd/hub', DesiredCapabilities::chrome()`
4. Execute a simple test like below
